import * as campaignServices from '../../src/features/campaign/services';
import campaigns from '../mock/campaigns';
import { GET_CAMPAIGNS } from './queries';

describe('Campaign type ', () => {
  let query;
  beforeEach(() => {
    const { query: mockQuery, getMockFunction } = utils;
    query = mockQuery;
    campaignServices.getCampaigns = jest.fn(getMockFunction(campaigns));
  });

  it('should returns all campaigns', async () => {
    const {
      data: { getCampaigns: queryResponse },
      errors,
    } = await query({ query: GET_CAMPAIGNS, variables: { filter: [], limit: 10 } });

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(campaigns.length);
    expect(campaigns.every(({ id }, index) => campaigns[index].id === id)).toEqual(true);
  });

  it('should returns single campaign', async () => {
    const mockFilter = { field: 'id', value: '1' };

    const { data, errors } = await query({
      query: GET_CAMPAIGNS,
      variables: { filter: [mockFilter], limit: 1 },
    });
    const { getCampaigns: queryResponse } = data;

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(1);
    expect(queryResponse[0].id).toEqual('1');
  });

  it('should returns an error', async () => {
    const mockFilter = { field: 'id' };

    const { errors } = await query({
      query: GET_CAMPAIGNS,
      variables: { filter: [mockFilter], limit: 1 },
    });

    expect(errors).toBeDefined();
  });
});
