import { gql } from 'apollo-server';

// eslint-disable-next-line import/prefer-default-export
export const GET_CAMPAIGNS = gql`
  query getCampaigns($filter: [Filter], $limit: Int, $offset: Int) {
    getCampaigns(filter: $filter, limit: $limit, offset: $offset) {
      id
      title
      description
      image
      startDate
      endDate
    }
  }
`;
