import * as categoryServices from '../../src/features/category/services';
import { GET_CATEGORIES } from './queries';
import categories from '../mock/categories';

describe('Category type ', () => {
  let query;
  beforeAll(() => {
    const { query: mockQuery, getMockFunction } = utils;
    query = mockQuery;
    categoryServices.getCategories = jest.fn(getMockFunction(categories));
  });

  it('should returns all categories', async () => {
    const {
      data: { getCategories: queryResponse },
      errors,
    } = await query({ query: GET_CATEGORIES, variables: { filter: [], limit: 10 } });

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(categories.length);
    expect(categories.every(({ id }, index) => categories[index].id === id)).toEqual(true);
  });

  it('should returns single category', async () => {
    const mockFilter = { field: 'id', value: '1' };

    const { data, errors } = await query({
      query: GET_CATEGORIES,
      variables: { filter: [mockFilter], limit: 1 },
    });
    const { getCategories: queryResponse } = data;

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(1);
    expect(queryResponse[0].id).toEqual('1');
  });

  it('should returns an error', async () => {
    const mockFilter = { field: 'id' };

    const { errors } = await query({
      query: GET_CATEGORIES,
      variables: { filter: [mockFilter], limit: 1 },
    });

    expect(errors).toBeDefined();
  });
});
