import { gql } from 'apollo-server';

// eslint-disable-next-line import/prefer-default-export
export const GET_CATEGORIES = gql`
  query categories($filter: [Filter], $limit: Int, $offset: Int) {
    getCategories(filter: $filter, limit: $limit, offset: $offset) {
      id
      name
    }
  }
`;
