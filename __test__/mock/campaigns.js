export default [
  {
    id: 1,
    startDate: '01/01/2019',
    endDate: '01/01/2019',
    title: 'test',
    description: 'test test',
    image: 'url',
    products: [1],
  },
  {
    id: 2,
    startDate: '05/01/2019',
    endDate: '25/01/2020',
    title: 'test',
    description: 'test test',
    image: 'url',
    products: [1],
  },
];
