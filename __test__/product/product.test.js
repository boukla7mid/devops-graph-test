import products from '../mock/product';
import * as productServices from '../../src/features/product/services';
import { GET_PRODUCTS } from './queries';

describe('Product type ', () => {
  let query;
  beforeEach(() => {
    const { query: mockQuery, getMockFunction } = utils;
    query = mockQuery;
    productServices.getProducts = jest.fn(getMockFunction(products));
  });

  it('should returns all products', async () => {
    const {
      data: { getProducts: queryResponse },
      errors,
    } = await query({ query: GET_PRODUCTS, variables: { filter: [], limit: 10 } });

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(products.length);
    expect(products.every(({ id }, index) => products[index].id === id)).toEqual(true);
  });

  it('should returns single product', async () => {
    const mockFilter = { field: 'id', value: '1' };

    const { data, errors } = await query({
      query: GET_PRODUCTS,
      variables: { filter: [mockFilter], limit: 1 },
    });
    const { getProducts: queryResponse } = data;

    expect(errors).toBeUndefined();
    expect(queryResponse.length).toEqual(1);
    expect(queryResponse[0].id).toEqual('1');
  });

  it('should returns an error', async () => {
    const mockFilter = { field: 'id' };

    const { errors } = await query({
      query: GET_PRODUCTS,
      variables: { filter: [mockFilter], limit: 1 },
    });

    expect(errors).toBeDefined();
  });
});
