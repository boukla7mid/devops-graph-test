import { gql } from 'apollo-server';

// eslint-disable-next-line import/prefer-default-export
export const GET_PRODUCTS = gql`
  query getProducts($filter: [Filter], $limit: Int, $offset: Int) {
    getProducts(filter: $filter, limit: $limit, offset: $offset) {
      id
      name
      description
      images
      unit
      type
    }
  }
`;
