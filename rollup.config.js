import resolve from 'rollup-plugin-node-resolve';
import builtins from 'rollup-plugin-node-builtins';
import babel from 'rollup-plugin-babel';

import { map, mapValues } from 'lodash';

const packages = require('./package.json');

const { dependencies } = packages;

const external = map(dependencies, (mod, key) => key);
const globals = mapValues(dependencies, (mod, key) => key);

export default {
  input: 'src/index.js',
  output: {
    name: 'motrio-graphql',
    file: 'dist/bundle.js',
    format: 'umd',
    globals,
  },
  external,
  plugins: [
    builtins(),
    babel({
      babelrc: false,
      presets: [
        [
          'env',
          {
            targets: {
              node: 'current',
            },
            modules: false,
          },
        ],
      ],
      plugins: [
        'transform-export-default',
        '@babel/plugin-proposal-object-rest-spread',
        [
          'module-resolver',
          {
            root: ['./'],
            alias: {
              utils: './src/utils',
            },
          },
        ],
      ],
      exclude: 'node_modules/**',
    }),
    resolve(),
  ],
};
