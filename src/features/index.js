import { gql } from 'apollo-server';
import { compact, isEmpty } from 'lodash';

import * as Product from './product';

const features = [Product];

const isMutations = Boolean(features.find(f => !isEmpty(f.Resolvers.Mutation)));
const isQueries = Boolean(features.find(f => !isEmpty(f.Resolvers.Query)));

const rootTypes = `
  scalar Date
  input Filter {
    field: String!
    value: String!
  }
`;

const stringDoers = field => features.reduce((acc, curr) => acc.concat(curr[field] || ''), '');
const resolversDoers = field =>
  features.reduce((acc, curr) => ({ ...acc, ...curr.Resolvers[field] }), {});

const typeDefsString = `
  ${rootTypes}
  ${stringDoers('Type')}
  ${stringDoers('InputType')}
  type Query {
  ${stringDoers('Query')}
  }
`;

const typeDefs = gql`
  ${typeDefsString}
`;


const Query = resolversDoers('Query');
const Mutation = resolversDoers('Mutation');
const typeResolvers = resolversDoers('TypeResolvers');


const resolvers = {};

if (isMutations) {
  resolvers.Mutation = Mutation;
}

if (isQueries) {
  resolvers.Query = Query;
}

export default {
  typeDefs,
  resolvers: { ...resolvers, ...typeResolvers },
};
