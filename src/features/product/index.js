import * as queries from './productQueries';
import ProductType from './productTypes';

export const { Query } = queries;

export const Type = ProductType;

export const InputType = '';

export const Mutation = '';

export const dataLoader = {
  name: 'productDataLoader',
};

export const Resolvers = {
  Query: {},
  Mutation: {},
  TypeResolvers: {},
};
