export const Query = `
  getProducts(filter: [Filter] = [], limit: Int = 10, offset: Int = 0): [Product]
  getProductById(productId: ID!): Product
  getProductsByCategories(
    categoryId: ID!,
    filter: [Filter] = [],
    limit: Int = 10,
    offset: Int = 0,
    ): [Product]
`;
