export default `
  enum AllowedProductType {
    media
    physical
  }

  type Product {
    id: ID
    name: String
    description: String
    brand: String
    price: Float
    unit: String
    unitPrice: Float
    type: AllowedProductType
    isCustomizable: Boolean
    isPopular: Boolean
    tips: String
    image: String
    countdown: Date
  }

`;
