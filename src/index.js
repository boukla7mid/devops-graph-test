import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import cookieParser from 'cookie-parser';
import schemas from './features';
import getEnv from './utils/getEnv';

const app = express();
app.use(cookieParser());
// todo remove http://localhost:3000 from whitelist
const whitelist = [getEnv('FRONT_ENDPOINT'), 'http://localhost:3000', 'http://localhost:4000'];

const corsOptions = {
  origin(origin, callback) {
    const originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, originIsWhitelisted);
  },
  credentials: true,
};



const server = new ApolloServer({
  ...schemas,
  mocks: true,
  context: ctx => ctx,
  playground: {
    settings: {
      'request.credentials': 'include',
    },
  },
  formatError: error => {
    // to do handle error
    return error;
  },
});

server.applyMiddleware({
  cors: corsOptions,
  app,
});

app.get('/healthz', (req, res) => {
  res.send('OK');
});

app.listen(7000, () => {
  /* eslint-disable  no-console */
  console.log(`🚀 application ready at ${getEnv('PORT')}`);
});
