import nodeFetch from 'node-fetch';
import DataLoader from 'dataloader';
import { stringify } from 'querystring';
import getEnv from './getEnv';

/**
 *
 * @param batchFunc
 * @returns {DataLoader<>}
 */
const getDataLoader = batchFunc => new DataLoader(keys => batchFunc(keys));

/**
 * Transform an array of filter to object to be passed as query params
 *
 * @param params {Array}
 * @returns {Object}
 */
const normalizeParams = params =>
  params
    .map(param => ({ [Object.values(param)[0]]: Object.values(param)[1] }))
    .reduce((acc, curr) => `${acc}&${stringify(curr)}`, '');

const defaultOptions = {
  method: 'GET',
};

const defaultHeaders = { 'Content-Type': 'application/json' };

/**
 * Wrapper for node-fetch to prevent redundancy
 * @returns {function(<String>, <Array>, <Object> = *): Promise<any>}
 * @param baseUrl
 */
const fetch = (baseUrl = '') => async (url, params, options = {}) => {
  const normalizedParams = Array.isArray(params) ? normalizeParams(params) : params;
  const { headers = {}, body, ...restOptions } = options;

  return nodeFetch(`${baseUrl}${url}?${normalizedParams}`, {
    ...defaultOptions,
    ...restOptions,
    body: body ? JSON.stringify(body) : null,
    headers: { ...headers, ...defaultHeaders },
  }).then(res => res.json());
};

const strapiBaseURL = new URL(`${getEnv('STRAPI_HOST')}/`);
const fetchFromStrapi = fetch(strapiBaseURL);
const cartNListBaseURL = new URL(`${getEnv('CART_AND_LIST_HOST')}/`);
const fetchFromCartNList = fetch(cartNListBaseURL);

export { fetchFromStrapi, fetchFromCartNList, fetch, getDataLoader };
