import { fetchFromStrapi, fetchFromCartNList, fetch, getDataLoader } from './fetch';
import getEnv from './getEnv';

export { fetchFromStrapi, fetchFromCartNList, fetch, getDataLoader, getEnv };
